<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/multilang?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_activer' => 'Multilang im Redaktionssystem aktivieren :',
	'cfg_boite_multilang' => 'Konfiguration des  Plugins Multilang',
	'cfg_descr_multilang' => 'Multilang : Plugin zur Verwaltung der multi-Tags',
	'cfg_effacer' => 'neu starten',
	'cfg_explication_crayons' => 'Setzt voraus, dass Multilang für den öffentlichen  Bereich der Website aktiviert ist.',
	'cfg_langues' => 'Verwendete Sprachen',
	'cfg_lbl_crayons' => 'Multilang für die Stifte verwenden',
	'cfg_lbl_espace_public' => 'Multilan für den öffentlichen Bereich der Website verwenden',
	'cfg_lbl_formstables' => 'für Formulare und Tabellen',
	'cfg_lbl_siteconfig' => 'für die Website-Konfiguration', # MODIF
	'cfg_titre_multilang' => 'Multilang',
	'champ_numero' => 'Nummer',
	'configuration_multilang' => 'Multilang',

	// E
	'explication_langues_utilisees' => 'Wähelen Sie die Sprachen für das Menü von Multilang. Wenn Sei keine auswählen, werden alle verwendet.',

	// L
	'label_langues_utilisees' => 'Von Multilang verwendete Sprachen',
	'lien_desactiver' => 'Alles',
	'lien_multi_title' => 'Mehrsprachige Felder in « @lang@ » bearbeiten',
	'lien_multi_title_sans' => 'Die Version « @lang@ » ist unvollständig. Klicken Sie zum Bearbeiten.',

	// M
	'message_champs_readonly' => 'Die mehrsprachigen Felder befinden sich im Nur-Lesen-Modus. Wählen Sie eine Sprache, um sie zu ändern.'
);
