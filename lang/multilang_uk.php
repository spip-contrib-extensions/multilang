<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/multilang?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_activer' => 'Увімкнути MultiLang на сторінці редагування:',
	'cfg_boite_multilang' => 'Конфігурація плагіна Multilang',
	'cfg_descr_multilang' => 'Multilang: Плагін для управління тегами multi (багатомовності)',
	'cfg_effacer' => 'Повторно ініціювати',
	'cfg_explication_crayons' => 'Потрібно, щоб багатомовність була активована в загальнодоступній області сайту.',
	'cfg_langues' => 'Мови, що використовуються',
	'cfg_lbl_crayons' => 'Використовувати multilang у crayons',
	'cfg_lbl_espace_public' => 'Використовувати багатомовність в загальнодоступній області сайту',
	'cfg_lbl_formstables' => 'форми і таблиці',
	'cfg_lbl_siteconfig' => 'конфігурації сайту', # MODIF
	'cfg_titre_multilang' => 'Multilang',
	'champ_numero' => 'Номер',
	'configuration_multilang' => 'Multilang',

	// E
	'explication_langues_utilisees' => 'Виберіть мову для використання в меню MultiLang. Виберіть «Ні», щоб використовувати усі.',

	// L
	'label_langues_utilisees' => 'Мови, що використовуються з multilang',
	'lien_desactiver' => 'Усі',
	'lien_multi_title' => 'Редагувати багатомовні поля в «@lang@»',
	'lien_multi_title_sans' => 'Версія «@lang@» не є повною. Натисніть, щоб змінити.',

	// M
	'message_champs_readonly' => 'Багатомовні поля тільки для читання, виберіть мову, щоб їх змінити'
);
