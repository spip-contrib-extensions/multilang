<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/multilang?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_activer' => 'Activate Multilang on the edit page:',
	'cfg_boite_multilang' => 'Configuration of the Multilang plugin',
	'cfg_descr_multilang' => 'Multilang: plugin to manage multi tags',
	'cfg_effacer' => 'Re-init',
	'cfg_explication_crayons' => 'Requires multilang to be activated in the public space of the site.',
	'cfg_formulaires_autres_explication' => 'Comma-separated jQuery selectors: <code>.formulaire_editer_patate, .formulaire_machiner_truc:not([bidule])</code>',
	'cfg_formulaires_autres_label' => 'Other forms',
	'cfg_formulaires_label' => 'Forms',
	'cfg_langues' => 'Used languages',
	'cfg_lbl_crayons' => 'Use multilang in the pencils',
	'cfg_lbl_espace_public' => 'Use multilang in the public space',
	'cfg_lbl_formstables' => 'of forms&tables',
	'cfg_lbl_siteconfig' => 'Website configuration (welcome and identity)',
	'cfg_titre_multilang' => 'Multilang',
	'champ_numero' => 'Number',
	'configuration_multilang' => 'Multilang',

	// E
	'explication_langues_utilisees' => 'Select the languages to use in the multilang menu. Select none to use them all.',

	// L
	'label_langues' => 'Langs',
	'label_langues_utilisees' => 'Used languages by multilang',
	'lien_desactiver' => 'All',
	'lien_multi_title' => 'Edit multilingual fields in «@lang@»',
	'lien_multi_title_sans' => 'Version "@lang@" is incomplete. Click to edit.',

	// M
	'message_champs_readonly' => 'The multilingual fields are in read only mode, choose a language to edit them'
);
