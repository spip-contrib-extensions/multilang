<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-multilang?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'multilang_description' => 'Ce plugin rajoute un menu de langues du type <code>[fr] [en] [it]</code> au-dessus de chaque formulaire, en fonction des langues activées dans la configuration du site.',
	'multilang_slogan' => 'Gérer l’affichage du contenu des tags multi par un menu'
);
